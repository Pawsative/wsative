<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="21 Year Old Fullstack Web, Lua, Node.js, mainly creating content for Garry's Mod.">
		<meta property="og:image" content="<?= $avatar; ?>">
		<meta property="og:image:type" content="image/jpeg">
		<meta property="og:image:width" content="512">
		<meta property="og:image:height" content="512">
		<meta property="og:description" content="21 Year Old Fullstack Web, Lua, Node.js, mainly creating content for Garry's Mod.">
		<meta property="og:title" content="<?= htmlspecialchars($name);?>">
		<title><?= htmlspecialchars($name); ?></title>
		<!-- Custom fonts for this theme -->
		<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
		<!-- Theme CSS -->
		<link href="css/freelancer.min.css" rel="stylesheet">
		<link href="css/pawsative.css" rel="stylesheet">
	</head>
	<body id="page-top">
		<div id='loadboi'>
			<div id='pluswrap'>
				<div class='plus'>
					<div class="d-flex justify-content-center">
						<div class="spinner-border text-danger" role="status" style="width: 5rem; height: 5rem;">
							<span class="sr-only">Loading...</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id='noshow' style="display: none;">
			<!-- Masthead -->
			<header class="masthead bg-primary text-white text-center">
				<div class="container d-flex align-items-center flex-column">
					<!-- Masthead Avatar Image -->
					<a href="https://steamcommunity.com/profiles/<?= $id; ?>"><img class="masthead-avatar mb-5 avatar" src="<?= $avatar; ?>" alt=""></a>
					<!-- Masthead Heading -->
					<h1 class="masthead-heading text-uppercase mb-0"><a href="<?= $avatar; ?>"><?= htmlspecialchars($name); ?></a></h1>
					<br/>
					<!-- Masthead Subheading -->
					<p class="masthead-subheading font-weight-light mb-0">21 years old, (G)Lua, Fullstack Web, Node.js, C# Developer</p>
					<p>Previous dev for: GangWarsRP, AceServers, TheScaryTaco, Alps Gaming, Mist Servers, Rival Gamers</p>
					<p>Known Languages: Lua, Fullstack Web (HTML, CSS, Javascript), Node.js, C#, SQL (Procedures/Queries)</p>
					<p class="template"><small>names/images on this website are synced with my Steam Profile using SteamAPI</small></p>
				</div>
			</header>
			<!-- Portfolio Section -->
			<section class="page-section portfolio" id="portfolio">
				<div class="container">
					<!-- Portfolio Section Heading -->
					<h2 class="page-section-heading text-center text-uppercase text-white">Past Work</h2><br/><br/>
					<!-- Portfolio Grid Items -->
					<h5 class="page-section-heading text-center text-blue">FiveM</h5><br/>
					<div class="row">
						<div class="col-md-6 col-lg-4">
							<div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal1_FiveM">
								<div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
									<div class="portfolio-item-caption-content text-center text-white">
										<i class="fas fa-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-fluid" src="img/portfolio/fivem/bank.jpg" alt="">
							</div>
						</div>
					</div>
          <br/><br/>
					<h5 class="page-section-heading text-center text-blue">Garry's Mod</h5>
          <br/>
					<div class="row">
						<!-- Portfolio Item 1 -->
						<div class="col-md-6 col-lg-4">
							<div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal1">
								<div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
									<div class="portfolio-item-caption-content text-center text-white">
										<i class="fas fa-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-fluid" src="img/portfolio/char_creation.jpg" alt="">
							</div>
						</div>
						<!-- Portfolio Item 2 -->
						<div class="col-md-6 col-lg-4">
							<div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal2">
								<div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
									<div class="portfolio-item-caption-content text-center text-white">
										<i class="fas fa-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-fluid" src="img/portfolio/gang_thing.jpg" alt="">
							</div>
						</div>
						<!-- Portfolio Item 3 -->
						<div class="col-md-6 col-lg-4">
							<div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal3">
								<div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
									<div class="portfolio-item-caption-content text-center text-white">
										<i class="fas fa-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-fluid" src="img/portfolio/ace_bot.png" alt="">
							</div>
						</div>
						<!-- Portfolio Item 4 -->
						<div class="col-md-6 col-lg-4">
							<div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal4">
								<div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
									<div class="portfolio-item-caption-content text-center text-white">
										<i class="fas fa-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-fluid" src="img/portfolio/taco_panel.png" alt="">
							</div>
						</div>
						<!-- Portfolio Item 5 -->
						<div class="col-md-6 col-lg-4">
							<div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal5">
								<div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
									<div class="portfolio-item-caption-content text-center text-white">
										<i class="fas fa-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-fluid" src="img/portfolio/uwu_bot.png" alt="">
							</div>
						</div>
						<!-- Portfolio Item 6 -->
						<div class="col-md-6 col-lg-4">
							<div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal6">
								<div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
									<div class="portfolio-item-caption-content text-center text-white">
										<i class="fas fa-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-fluid" src="img/portfolio/ace_index.png" alt="">
							</div>
						</div>
						<div class="col-md-6 col-lg-4">
							<div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal7">
								<div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
									<div class="portfolio-item-caption-content text-center text-white">
										<i class="fas fa-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-fluid" src="img/portfolio/cityrp3.jpg" alt="">
							</div>
						</div>
						<div class="col-md-6 col-lg-4">
							<div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal8">
								<div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
									<div class="portfolio-item-caption-content text-center text-white">
										<i class="fas fa-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-fluid" src="img/portfolio/alps_gang1.jpg" alt="">
							</div>
						</div>
						<div class="col-md-6 col-lg-4">
							<div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal9">
								<div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
									<div class="portfolio-item-caption-content text-center text-white">
										<i class="fas fa-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-fluid" src="img/portfolio/alps_cityrp9.jpg" alt="">
							</div>
						</div>
					</div>
					<p class="text-white">To see more work, message me on either Steam or Discord, most of my work was for other communities and being backend, so I don't really have screenshots of those. All my projects use Gitlab, and using Gitlab CI/CD for automatic updates upon merge from a dev branch to master branch.</p>
					<!-- /.row -->
				</div>
			</section>
			<!-- Contact Section -->
			<section class="page-section bg-primary text-white mb-0 contact_me" id="contact">
				<div class="container">
					<!-- About Section Heading -->
					<h2 class="page-section-heading text-center text-uppercase text-white">Contact</h2>
					<!-- About Section Content -->
					<div class='centerorsomething'>
						<div class='social'>
							<a target="_blank" href="https://github.com/pawsative/"><i class="fab fa-github"></i>&nbsp; Github</a>
							<a target="_blank" href="https://gitlab.com/pawsative/"><i class="fab fa-gitlab"></i>&nbsp; Gitlab</a>
							<a target="_blank" href="<?= $url; ?>"><i class="fab fa-steam"></i>&nbsp; Steam</a>
							<a href="#" id="discord_link"><i class="fab fa-discord"></i>&nbsp; Discord</a>
						</div>
					</div>
				</div>
			</section>
			<!-- Copyright Section -->
			<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
			<div class="scroll-to-top d-lg-none position-fixed ">
				<a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
				<i class="fa fa-chevron-up"></i>
				</a>
			</div>
		</div>
		<!-- Portfolio Modals -->
		<!-- Portfolio Modal 1 -->
		<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-labelledby="portfolioModal1Label" aria-hidden="true">
			<div class="modal-dialog modal-xl" role="document">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
					<i class="fas fa-times"></i>
					</span>
					</button>
					<div class="modal-body text-center">
						<div class="container">
							<div class="row justify-content-center">
								<div class="col-lg-8">
									<!-- Portfolio Modal - Title -->
									<h2 class="page-section-heading text-center text-uppercase text-white">Character Creation</h2>
									<!-- Icon Divider -->
									<!-- Portfolio Modal - Image -->
									<img class="img-fluid rounded mb-5" src="img/portfolio/char_creation.jpg" loading="lazy" alt="">
									<img class="img-fluid rounded mb-5" src="img/portfolio/char_creation2.jpg" loading="lazy" alt="">
									<!-- Portfolio Modal - Text -->
									<p class="mb-5">Character creation for a WIP Garry's Mod gamemode, feauturing multiple characters with different inventories, money, banks, phonenumbers etc.<br/><br/>Languages: Lua, SQL (Procedures)</p>
									<button class="btn btn-danger" href="#" data-dismiss="modal">
									<i class="fas fa-times fa-fw"></i>
									Close Window
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Portfolio Modal 2 -->
		<div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-labelledby="portfolioModal2Label" aria-hidden="true">
			<div class="modal-dialog modal-xl" role="document">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
					<i class="fas fa-times"></i>
					</span>
					</button>
					<div class="modal-body text-center">
						<div class="container">
							<div class="row justify-content-center">
								<div class="col-lg-8">
									<!-- Portfolio Modal - Title -->
									<h2 class="page-section-heading text-center text-uppercase text-white">Ace Gang System</h2>
									<!-- Icon Divider -->
									<!-- Portfolio Modal - Image -->
									<img class="img-fluid rounded mb-5" src="img/portfolio/gang_thing.jpg" loading="lazy" alt="">
									<!-- Portfolio Modal - Text -->
									<p class="mb-5">Advanced Gang System, featuring fighting other gangs for Gang Points, monthly reset of gang points and gives top 3 gangs with most points special rewards, such as colored name tags etc.<br/><br/>Languages: Lua, SQL (Procedures)</p>
									<button class="btn btn-danger" href="#" data-dismiss="modal">
									<i class="fas fa-times fa-fw"></i>
									Close Window
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Portfolio Modal 3 -->
		<div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-labelledby="portfolioModal3Label" aria-hidden="true">
			<div class="modal-dialog modal-xl" role="document">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
					<i class="fas fa-times"></i>
					</span>
					</button>
					<div class="modal-body text-center">
						<div class="container">
							<div class="row justify-content-center">
								<div class="col-lg-8">
									<!-- Portfolio Modal - Title -->
									<h2 class="page-section-heading text-center text-uppercase text-white">Ace Bot</h2>
									<!-- Icon Divider -->
									<!-- Portfolio Modal - Image -->
									<img class="img-fluid rounded mb-5" src="img/portfolio/ace_bot.png" loading="lazy" alt="">
									<!-- Portfolio Modal - Text -->
									<p class="mb-5">Discord Bot to get real time information from the server, such as Player Information, Ban Information, Server Information, Players/Administrators Online<br/><br/>Languages Used: Node.js, SQL (Procedures)</p>
									<button class="btn btn-danger" href="#" data-dismiss="modal">
									<i class="fas fa-times fa-fw"></i>
									Close Window
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Portfolio Modal 4 -->
		<div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-labelledby="portfolioModal4Label" aria-hidden="true">
			<div class="modal-dialog modal-xl" role="document">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
					<i class="fas fa-times"></i>
					</span>
					</button>
					<div class="modal-body text-center">
						<div class="container">
							<div class="row justify-content-center">
								<div class="col-lg-8">
									<!-- Portfolio Modal - Title -->
									<h2 class="page-section-heading text-center text-uppercase text-white">Taco/Ace Panel</h2>
									<!-- Icon Divider -->
									<!-- Portfolio Modal - Image -->
									<img class="img-fluid rounded mb-5" src="img/portfolio/taco_panel.png" loading="lazy" alt="">
									<img class="img-fluid rounded mb-5" src="img/portfolio/taco_panel2.png" loading="lazy">
									<!-- Portfolio Modal - Text -->
									<p class="mb-5">Web Administraton Panel for mine, and a friends Garry's Mod community administrators, to edit players bans, player data such as money, rank, add bans, remove warnings, add warnings, reset their data etc.<br/><br/>Languages Used: Node.js, PHP, Front Web Markup (HTML,CSS etc), JS & Vue.js</p>
									<button class="btn btn-danger" href="#" data-dismiss="modal">
									<i class="fas fa-times fa-fw"></i>
									Close Window
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Portfolio Modal 5 -->
		<div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-labelledby="portfolioModal5Label" aria-hidden="true">
			<div class="modal-dialog modal-xl" role="document">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
					<i class="fas fa-times"></i>
					</span>
					</button>
					<div class="modal-body text-center">
						<div class="container">
							<div class="row justify-content-center">
								<div class="col-lg-8">
									<!-- Portfolio Modal - Title -->
									<h2 class="page-section-heading text-center text-uppercase text-white">UwU Bot <small>(god, why)</small></h2>
									<!-- Icon Divider -->
									<!-- Portfolio Modal - Image -->
									<img class="img-fluid rounded mb-5" src="img/portfolio/uwu_bot.png" loading="lazy" alt="">
									<!-- Portfolio Modal - Text -->
									<p class="mb-5">Private discord bot made for friends because a certain music bot kept going offline <small>(and someone kept complaining, no names)</small>, features are meme commands, music playing, auto kick members without certain role after 24 hours.<br/><br/>Languages: Node.js, SQL (Procedures)</p>
									<button class="btn btn-danger" href="#" data-dismiss="modal">
									<i class="fas fa-times fa-fw"></i>
									Close Window
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Portfolio Modal 6 -->
		<div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-labelledby="portfolioModal6Label" aria-hidden="true">
			<div class="modal-dialog modal-xl" role="document">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
					<i class="fas fa-times"></i>
					</span>
					</button>
					<div class="modal-body text-center">
						<div class="container">
							<div class="row justify-content-center">
								<div class="col-lg-8">
									<!-- Portfolio Modal - Title -->
									<h2 class="page-section-heading text-center text-uppercase text-white">Ace - Index</h2>
									<p><small>no, not arch's shitty version that used mysqli and slow ass page speed</small></p>
									<!-- Icon Divider -->
									<!-- Portfolio Modal - Image -->
									<img class="img-fluid rounded mb-5" src="img/portfolio/ace_index.png" loading="lazy" alt="">
									<!-- Portfolio Modal - Text -->
									<p class="mb-5">Index page for Ace Servers, all data is real-time and served via a Express app on a VPS for near instant page loading.<br/><br/>Languages Used:</p>
									<ul>
										<li>Node.js</li>
										<li>PHP</li>
										<li>Front Web Markup (HTML,CSS, JS)</li>
										<li>Express</li>
										<li>SQL (Procedures)</li>
									</ul>
									<button class="btn btn-danger" href="#" data-dismiss="modal">
									<i class="fas fa-times fa-fw"></i>
									Close Window
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="portfolio-modal modal fade" id="portfolioModal7" tabindex="-1" role="dialog" aria-labelledby="portfolioModal7Label" aria-hidden="true">
			<div class="modal-dialog modal-xl" role="document">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
					<i class="fas fa-times"></i>
					</span>
					</button>
					<div class="modal-body text-center">
						<div class="container">
							<div class="row justify-content-center">
								<div class="col-lg-8">
									<!-- Portfolio Modal - Title -->
									<h2 class="page-section-heading text-center text-uppercase text-white">CityRP3 (v1.0)</h2>
									<!-- Icon Divider -->
									<!-- Portfolio Modal - Image -->
									<img class="img-fluid rounded mb-5" src="img/portfolio/cityrp3.jpg" loading="lazy" alt="">
									<!-- Portfolio Modal - Text -->
									<p class="mb-5">Full custom gamemode in Garry's Mod, featuring whitelisted EMS/Fire/Police, realistic Gangs. Gamemode had full HTML interface with HTML/CSS for optimization in UI menus, and for ease of updating the UI's. Players could edit the CSS to design the menu how they liked it.<br/><br/>Languages Used: Node.js, PHP, Front Web Markup (HTML,CSS etc), JS, SQL (Procedures)</p>
									<button class="btn btn-danger" href="#" data-dismiss="modal">
									<i class="fas fa-times fa-fw"></i>
									Close Window
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="portfolio-modal modal fade" id="portfolioModal8" tabindex="-1" role="dialog" aria-labelledby="portfolioModal7Label" aria-hidden="true">
			<div class="modal-dialog modal-xl" role="document">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
					<i class="fas fa-times"></i>
					</span>
					</button>
					<div class="modal-body text-center">
						<div class="container">
							<div class="row justify-content-center">
								<div class="col-lg-8">
									<!-- Portfolio Modal - Title -->
									<h2 class="page-section-heading text-center text-uppercase text-white">Alps - Gangs</h2>
									<!-- Icon Divider -->
									<!-- Portfolio Modal - Image -->
									<img class="img-fluid rounded mb-5" src="img/portfolio/alps_gang1.jpg" loading="lazy" alt="">
									<img class="img-fluid rounded mb-5" src="img/portfolio/alps_gang2.jpg" loading="lazy" alt="">
									<!-- Portfolio Modal - Text -->
									<p class="mb-5">Re-made gang system, using <a href="https://gitlab.com/sleeppyy/xeninui">XeninUI</a> featuring: Gang vs Gang fighting, Gang Points, Upgrades, Gang Bank, Territories, monthly rewards for gangs with top points etc.<br/><br/>Languages Used: Lua, SQL (Procedures)</p>
									<button class="btn btn-danger" href="#" data-dismiss="modal">
									<i class="fas fa-times fa-fw"></i>
									Close Window
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="portfolio-modal modal fade" id="portfolioModal9" tabindex="-1" role="dialog" aria-labelledby="portfolioModal7Label" aria-hidden="true">
			<div class="modal-dialog modal-xl" role="document">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
					<i class="fas fa-times"></i>
					</span>
					</button>
					<div class="modal-body text-center">
						<div class="container">
							<div class="row justify-content-center">
								<div class="col-lg-8">
									<!-- Portfolio Modal - Title -->
									<h2 class="page-section-heading text-center text-uppercase text-white">CityRP3 - Full Custom Gamemode <small>(V2.0)</small></h2>
									<!-- Icon Divider -->
									<!-- Portfolio Modal - Image -->
									<img class="img-fluid rounded mb-5" src="img/portfolio/alps_cityrp1.jpg" loading="lazy" alt="">
									<img class="img-fluid rounded mb-5" src="img/portfolio/alps_cityrp2.jpg" loading="lazy" alt="">
									<img class="img-fluid rounded mb-5" src="img/portfolio/alps_cityrp3.jpg" loading="lazy" alt="">
									<img class="img-fluid rounded mb-5" src="img/portfolio/alps_cityrp4.jpg" loading="lazy" alt="">
									<img class="img-fluid rounded mb-5" src="img/portfolio/alps_cityrp5.jpg" loading="lazy" alt="">
									<img class="img-fluid rounded mb-5" src="img/portfolio/alps_cityrp6.jpg" loading="lazy" alt="">
									<img class="img-fluid rounded mb-5" src="img/portfolio/alps_cityrp7.jpg" loading="lazy" alt="">
									<img class="img-fluid rounded mb-5" src="img/portfolio/alps_cityrp8.jpg" loading="lazy" alt="">
									<img class="img-fluid rounded mb-5" src="img/portfolio/alps_cityrp9.jpg" loading="lazy" alt="">
									<!-- Portfolio Modal - Text -->
									<p class="mb-5">Full custom gamemode, made for https://alps.gg featuring full HTML menus, made for optimization. <br/>Features: 
									<ul>
										<li>HTML Menus</li>
										<li>License Plates for Vehicles, unique & customizabel</li>
										<li>Clan System</li>
										<li>Custom admin system</li>
										<li>Realistic Paramedics/Defliberators</li>
										<li>Realistic fires/firemen</li>
									</ul>
									Took a week and a half to make.<br/><br/>Languages Used: Lua, SQL (Procedures), Fullstack Web (HTML/CSS/Javascript)<br/><br/>Also, had to learn Linux to setup <a href="https://pterodactyl.io/">Pterodactyl Panel</a> and a <a href="https://alps.gg">Web Host</a></p>
									<button class="btn btn-danger" href="#" data-dismiss="modal">
									<i class="fas fa-times fa-fw"></i>
									Close Window
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Fivem Modals -->
		<div class="portfolio-modal modal fade" id="portfolioModal1_FiveM" tabindex="-1" role="dialog" aria-labelledby="portfolioModal7Label" aria-hidden="true">
			<div class="modal-dialog modal-xl" role="document">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
					<i class="fas fa-times"></i>
					</span>
					</button>
					<div class="modal-body text-center">
						<div class="container">
							<div class="row justify-content-center">
								<div class="col-lg-8">
									<!-- Portfolio Modal - Title -->
									<h2 class="page-section-heading text-center text-uppercase text-white">TropixRP - Bank System</small></h2>
									<!-- Icon Divider -->
									<!-- Portfolio Modal - Image -->
									<img class="img-fluid rounded mb-5" src="img/portfolio/fivem/bank.jpg" loading="lazy" alt="">
									<img class="img-fluid rounded mb-5" src="img/portfolio/fivem/bank2.jpg" loading="lazy" alt="">
									<img class="img-fluid rounded mb-5" src="img/portfolio/fivem/bank3.jpg" loading="lazy" alt="">
									<!-- Portfolio Modal - Text -->
									<p class="mb-5">Full custom banking system, made for https://tropixrp.com<br/>Features: 
									<ul>
										<li>HTML/Javascript Menus</li>
										<li>Share 'Personal' bank accounts with other players</li>
										<li>Manage company account</li>
										<li>Very optimized/very fast, less than 1 second response from client -> server -> client</li>
									</ul>
									Took 2 days to to make.<br/><br/>Languages Used: Lua, SQL (Procedures), Fullstack Web (HTML/CSS/Javascript)<br/></p>
									<button class="btn btn-danger" href="#" data-dismiss="modal">
									<i class="fas fa-times fa-fw"></i>
									Close Window
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Bootstrap core JavaScript -->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<!-- Plugin JavaScript -->
		<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
		<!-- Contact Form JavaScript -->
		<script src="js/jqBootstrapValidation.js"></script>
		<script src="js/contact_me.js"></script>
		<!-- Custom scripts for this template -->
		<script src="js/freelancer.min.js"></script>
		<script src="js/particles.min.js"></script>
		<script src="js/start.js"></script>
	</body>
</html>