<?php include_once 'config.php'; ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="21 Year Old Fullstack Web, Lua, Node.js, mainly creating content for Garry's Mod.">
		<meta property="og:image" content="<?= $avatar; ?>">
		<meta property="og:image:type" content="image/jpeg">
		<meta property="og:image:width" content="512">
		<meta property="og:image:height" content="512">
		<meta property="og:description" content="21 Year Old Fullstack Web, Lua, Node.js, mainly creating content for Garry's Mod.">
		<meta property="og:title" content="<?= htmlspecialchars($name);?>">
		<title><?= htmlspecialchars($name); ?></title>
		<!-- Custom fonts for this theme -->
		<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
		<!-- Theme CSS -->
		<link href="css/freelancer.min.css" rel="stylesheet">
		<link href="css/pawsative.css" rel="stylesheet">
	</head>
	<body id="page-top">
		<div id='loadboi'>
			<div id='pluswrap'>
				<div class='plus'>
					<div class="d-flex justify-content-center">
						<div class="spinner-border text-danger" role="status" style="width: 5rem; height: 5rem;">
							<span class="sr-only">Loading...</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id='noshow' style="display: none;">
			<!-- Masthead -->
			<header class="masthead bg-primary text-white text-center">
				<div class="container d-flex align-items-center flex-column">
					<!-- Masthead Avatar Image -->
					<a href="https://steamcommunity.com/profiles/<?= $id; ?>"><img class="masthead-avatar mb-5 avatar" src="<?= $avatar; ?>" alt=""></a>
					<!-- Masthead Heading -->
					<h1 class="masthead-heading text-uppercase mb-0"><a href="<?= $avatar; ?>"><?= htmlspecialchars($name); ?></a></h1>
					<br/>
					<!-- Masthead Subheading -->
					<p class="masthead-subheading font-weight-light mb-0">22 years old Fullstack Developer</p>
					<p>Previous developer for: GangWarsRP, AceServers, TheScaryTaco, Alps Gaming, Mist Servers, Rival Gamers</p>
					<p>Known Coding Languages: Lua, Fullstack Web (HTML, CSS, Javascript), Node.js (Vue.js, React.js), C#, SQL (Procedures/Queries)</p>
                    <p>Known Skills/Software: Git, MySQL Server Management (MySQL Server, MariaDB), Linux Server Management, Windows Server Management</p> 
				</div>
			</header>
            <br/>
			<!-- Portfolio Section -->
			<section class="page-section portfolio" id="portfolio">
				<div class="container">
					<!-- Portfolio Section Heading -->
					<h2 class="page-section-heading text-center text-uppercase text-white">Past Work</h2><br/><br/>
					<!-- Portfolio Grid Items -->
					<h5 class="page-section-heading text-center text-blue">FiveM</h5><br/>
					<div class="row" id="fivem-work">
                        <!-- fivem shit here -->
					</div>
            <br/><br/>
					<h5 class="page-section-heading text-center text-blue">Garry's Mod</h5>
            <br/>
					<div class="row" id='gmod-work'>
						
					</div>
					<p class="text-white">To see more work, message me on either Steam or Discord, most of my work was for other communities and being backend, so I don't really have screenshots of those. All my projects use Gitlab, and using Gitlab CI/CD for automatic updates upon merge from a dev branch to master branch.</p>
					<!-- /.row -->
				</div>
			</section>
			<!-- Contact Section -->
			<section class="page-section bg-primary text-white mb-0 contact_me" id="contact">
				<div class="container">
					<!-- About Section Heading -->
					<h2 class="page-section-heading text-center text-uppercase text-white">Contact & Links</h2>
					<!-- About Section Content -->
					<div class='centerorsomething'>
						<div class='social'>
							<a target="_blank" href="https://github.com/pawsative/"><i class="fab fa-github"></i>&nbsp; Github</a>
							<a target="_blank" href="https://gitlab.com/pawsative/"><i class="fab fa-gitlab"></i>&nbsp; Gitlab</a>
							<a target="_blank" href="<?= $url; ?>"><i class="fab fa-steam"></i>&nbsp; Steam</a>
							<a href="#" id="discord_link"><i class="fab fa-discord"></i>&nbsp; Discord</a>
						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- Portfolio Modals -->
		<!-- Portfolio Modal 1 -->
        <div id="portfolio_modals">
            <div class='portfolio-modal modal fade' id='123123123123' tabindex='-1' role='dialog' aria-labelledby='portfolioModal7Label' aria-hidden='true'>
                <div class='modal-dialog modal-xl' role='document'>
                    <div class='modal-content'>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                            <span aria-hidden='true'>
                            <i class='fas fa-times'></i>
                            </span>
                        </button>
                        <div class='modal-body text-center'>
                            <div class='container'>
                                <div class='row justify-content-center'>
                                    <div class='col-lg-8'>
                                        <!-- Portfolio Modal - Title -->
                                        <h1 class='page-section-heading text-center text-uppercase text-dgrey' id="modal-portfolio-title">a</h1>
                                        <hr class='modal_line'/>
                                        <!-- Icon Divider -->
                                        <!-- Portfolio Modal - Image -->
                                        <h2 class='text-lgrey'>Summary:</h2>
                                        <div id='modal-portfolio-text'>
                                        </div>
                                        <hr class='modal_line'/>
                                        <h2 class='text-lgrey'>Media:</h1>
                                        <div id='modal-portfolio-images'>
                                        </div>
                                        <!-- Portfolio Modal - Text -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<!-- Fivem Modals -->
		<!-- Bootstrap core JavaScript -->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<!-- Plugin JavaScript -->
		<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
		<!-- Contact Form JavaScript -->
		<script src="js/jqBootstrapValidation.js"></script>
		<script src="js/contact_me.js"></script>
		<!-- Custom scripts for this template -->
		<script src="js/freelancer.min.js"></script>
		<script src="js/particles.min.js"></script>
		<script src="js/start.js"></script>
        <script src="js/pawsative.dev.js"></script>
	</body>
</html>